<!DOCTYPE html>
<html>
<head>
    <title>Matriz Generada</title>
</head>
<body>
    <h1>Matriz Generada</h1>
    <?php
    
    define('TAMANO_MATRIZ', 3); 

    function generarMatrizCuadrada($tamano) {
        $matriz = array();
        for ($i = 0; $i < $tamano; $i++) {
            $fila = array();
            for ($j = 0; $j < $tamano; $j++) {
                $fila[] = rand(0, 9);
            }
            $matriz[] = $fila;
        }
        return $matriz;
    }

    function imprimirMatriz($matriz) {
        echo "<table border='1'>";
        foreach ($matriz as $fila) {
            echo "<tr>";
            foreach ($fila as $valor) {
                echo "<td>$valor</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }

    function sumaDiagonalPrincipal($matriz) {
        $suma = 0;
        for ($i = 0; $i < count($matriz); $i++) {
            $suma += $matriz[$i][$i];
        }
        return $suma;
    }

    while (true) {
        $matriz = generarMatrizCuadrada(TAMANO_MATRIZ);
        echo "<h2>Matriz generada:</h2>";
        imprimirMatriz($matriz);
        
        $sumaDiagonal = sumaDiagonalPrincipal($matriz);
        echo "<p>Suma de la diagonal principal: $sumaDiagonal</p>";
        
        if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
            echo "<p>La suma de la diagonal principal ($sumaDiagonal) está entre 10 y 15. Finalizando el script.</p>";
            break;
        } else {
            echo "<p>La suma de la diagonal principal no está en el rango deseado. Generando otra matriz...</p>";
        }
    }
    ?>
</body>
</html>
